# About this project #

Package | Version
:-------: | ------:
yarn       | [![yarn version](https://badge.fury.io/js/yarn.svg)](https://badge.fury.io/js/yarn)
cypress    | [12.15.0](https://docs.cypress.io/guides/references/changelog#12-15-0)


The purpose of this automation project is to create ui tests for Symphony test task test.


## Requirements ##

install Nodejs -> [nodejs](https://nodejs.org/en/) 

Validate the installation:
```shell
node -v
````
example:
```
$ v13.12.0
```
and
```
yarn -v
```
example:
```
$ 6.14.4
```

## Install dependencies ##

Run
```shell
yarn install
```

## Run tests ##

Open Cypress GUI to run test by spec file:

```shell
yarn cy:open
```

Run all test suite on headless mode and generate html mochawesome report:

```shell
yarn cy:run
```

To see the generated report open /mochawesome-report/mochawesome.html cath the file and open in your browser 

## Run CI/CD test ##

Open the repository page, go to Build -> Pipeline -> Run Pipeline -> select branch: Main -> run pipelne
