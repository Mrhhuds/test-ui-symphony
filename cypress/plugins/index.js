/// <reference types="cypress" />

const fs = require("fs-extra")
const path = require("path")
const { verifyDownloadTasks } =require('cy-verify-downloads')

const fetchConfigFile = file => {
    const configPath = `config/cypress.${file}.json`
    return (file && fs.readJson(path.join(__dirname, "../", configPath)))
}

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
/**
 * @type {Cypress.PluginConfig}
 */

module.exports = (on, config) => {
    // `on` is used to hook into various events Cypress emits
    // `config` is the resolved Cypress config
    const environment = config.env.configFile || "local"
    const configEnv = fetchConfigFile(environment)
    on('task', verifyDownloadTasks)
    return configEnv || config
}