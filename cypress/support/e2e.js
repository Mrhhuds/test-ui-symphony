Cypress.Commands.add('userLogin', () => {
    const user = Cypress.env('user')
    const pass = Cypress.env('pass')
    cy.visit('/')
    cy.get('[data-test="username"]')
		.click()
		.type(user)
    cy.get('[data-test="password"]')
		.click()
		.type(pass)
    cy.get('[data-test="login-button"]')
		.click()
})