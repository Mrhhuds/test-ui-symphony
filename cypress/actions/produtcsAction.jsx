export function selectAtoZ(element, sort){
    cy.get(element).as('select')
		.select(sort)
	cy.get('@select').should('have.text', 'Name (A to Z)Name (Z to A)Price (low to high)Price (high to low)')
}

export function assertSort(element, text){
    cy.get(element)
		.first()
		.should('have.visible.text', text)
}