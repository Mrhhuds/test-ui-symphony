/// <reference types="cypress" />

import { assertSort, selectAtoZ } from "../actions/produtcsAction"

describe('Order products', () => {

  beforeEach('made login', () => {
    cy.userLogin()
  })

  it('Assert order products A -> Z', () => {
	selectAtoZ('[data-test="product_sort_container"]', 'Name (A to Z)')
	assertSort('.inventory_item_name', 'Sauce Labs Backpack')
  })

  it('order and assert products Z -> A', () => {
	selectAtoZ('[data-test="product_sort_container"]', 'Name (Z to A)')
	assertSort('.inventory_item_name', 'Test.allTheThings() T-Shirt (Red)')
  })
})
